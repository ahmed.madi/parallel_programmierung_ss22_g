- **Teil 1**

1. 
Wir haben OpenMP für die Parallelisierung des Programms gewählt.

Mit hilfe von time  command in Linux kann man bestimmen, wie lange die Ausführung eines bestimmten Befehls dauert.
Bei dem C Program dauert es 0,004 Sekunden vor der Parallelisierung.

![Nicht_paral](/uploads/aff3c8df783bd246eb8ac098d0e36ab1/Nicht_paral.JPG)

Dann fangen wir mal mit der Parallelisierung.
Zuerst ist es wichtig ( #include <omp.h>) zu verwenden, um die OpenMP Bibliothek aufzurufen.

Nach der Parallelisierung wurden die folgenden Ergebnisse erzielt:

![Paral](/uploads/8e59a3111835431df052d68a2b727e55/Paral.JPG)

Das bedeutet, dass wir eine kleine  Zeitverbesserung mit der gegebene Matrix erreicht haben.

2. Wir haben versucht, das Programm mit einer Matrix von 1440x1440 laufen zu lassen, aber wir erhalten immer die gleiche Zeit-Ergebnisse von 0,004 Sekunden.
